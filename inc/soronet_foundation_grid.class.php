<?php class soronet_foundation_grid {
    const text_domain = 'soronet_foundation_grid';
    const post_type = 'soro_foundation_grid';
    const nonce_name = 'soro_foundation_grid_nonce';
    const nonce_action = 'soro_foundation_grid_display';
    const prefix = '_sorogrid_';

    function __construct(){
        $this->create_post_type();

        add_action('edit_form_after_title',array($this,'display_form'));
        add_action('save_post',array($this,'save'));
	    add_action('save_post',array($this,'save_used_in'));
	    add_action('admin_menu',array($this,'register_options_page'));
	    add_action('admin_init',array($this,'init_options'));
	    add_action('add_meta_boxes',array($this,'register_metaboxes'));
        add_shortcode('grid',array($this,'shortcode'));
    }
    function create_post_type(){
        register_post_type(self::post_type,array(
            'label' => __('Grids',self::text_domain),
            'public' => false,
            'show_ui' => true,
            'menu_position' => 20,
            'supports' => array('title'),
        ));
    }
	function register_options_page(){
		add_submenu_page('options-general.php','sorogrid_settings',__('Sorogrid Settings',self::text_domain),'manage_options','sorogrid-settings-page',
			array($this,'options_page'));
	}
	function register_metaboxes(){
		add_meta_box('used_in',__('Appears in',self::text_domain),array($this,'render_metabox'),self::post_type,'side','high');
	}
	function options_page(){
		echo '<h1>' . __('Sorogrid Settings',self::text_domain) . '</h1>';
		echo '<div class="wrap">';

		echo '<form method="post" action="options.php">';
		settings_fields('sorogrid-settings-page');
		do_settings_sections('sorogrid_settings');
		submit_button();

		echo '</form>';

		echo '</div>';
	}
	function init_options(){
		add_settings_section(
			'sorogrid_settings_main',
			__('Sorogrid General Options',self::text_domain),
			array($this,'init_options_section_callback'),
			'sorogrid_settings'
		);
		register_setting('sorogrid-settings-page','load_foundation');
		add_settings_field(
			'load_foundation',
			'Load Foundation Grid',
			array($this,'init_options_setting_callback'),
			'sorogrid_settings',
			'sorogrid_settings_main'
		);
	}
	function init_options_section_callback(){
	}

	function init_options_setting_callback() {
		echo '<input name="load_foundation" type="checkbox" value="1" ' . checked( 1, get_option( 'load_foundation' ), false ) . ' /> Enable if not included with theme';
	}

    function display_form($post){
        if($this->get_current_post_type() != self::post_type) return;

        wp_nonce_field(self::nonce_action,self::nonce_name);

        $this->style();

        echo '<input type="hidden" name="full_edit_mode" value="true" />';

	    echo $this->get_set_id($post->ID);
        echo '<br />';

        echo '<label>'.__('Columns: ',self::text_domain).' </label>';

        $cols =(int) get_post_meta($post->ID,self::prefix.'cols',true);
        echo '<input id="cols" type="text" name="'.self::prefix.'cols'.'" value="'.$cols.'" >';

        echo '<br />';
        echo '<label>'.__('Rows: ',self::text_domain).' </label>';
        echo '<input id="rows" type="text" name="'.self::prefix.'rows'.'" value="'.get_post_meta($post->ID,self::prefix.'rows',true).'" >';

        $items = get_post_meta($post->ID,self::prefix.'items',true);
        echo '<br />';

        echo '<label>'.__('Class(es): ',self::text_domain).' </label>';
        $classes = get_post_meta($post->ID,self::prefix.'classes',true);
        echo '<input id="classes" type="text" name="'.self::prefix.'classes'.'" value="'.$classes.'" >';
        echo '<span> ' . __('Optional classes separated by spaces',self::text_domain) . '</span>';


        echo '<div id="editors-wrap">';

        $this->draw_editors($cols,$items,$post->ID);

        echo '</div>';

        echo '<div id="modal" class="preview reveal-modal" data-reveal>';
        echo $this->draw_grid($cols,$items,$post->ID);
        echo '</div>';
    }

	function render_metabox($post){
		$used_in = get_post_meta($post->ID,'_used_in');
		if($used_in){
			foreach($used_in as $val){
				$p = get_post($val);
				echo '<a href="';
				echo get_permalink($p->ID);
				echo '" target="_blank" >' . $p->post_name . '</a>';
				echo '<br>';
			}
		} else {
			echo '<p> ' . __('Grid not in use yet',self::text_domain) . '</p>';
		}
		$id = get_post_meta($post->ID,self::prefix.'id',true);
		echo '<p>' . __('Paste this shortcode into any post or page content: ',self::text_domain) . '</p>';
		echo '<p>' . '[grid name="' . $id .'"]' . '</p>';

		echo '<a class="button" href="#" data-reveal-id="modal">'.__('PREVIEW',self::text_domain) .' </a>';
	}

    function draw_editors($cols,$items,$post_id){
        if($items > 0){
            echo '<p>';
            for($i = 0, $colCount = 0, $sum=0; $i < $items; $i++){

                if($colCount == 0) echo '<div class="sliderRow">';

                $width = get_post_meta($post_id,self::prefix.'editor_width_'.$i,true);
                $width = empty($width) ?  (int)(12/$cols) : $width;
                echo '<div class="slider">';
                echo '<input type="hidden" name="'.self::prefix.'editor_width['.$i.']" value="'.$width.'"/>';
                echo '<br />';
                echo'<span class="width">'.$width.'</span>';
                echo'</div>';
                $colCount++;
                $sum += $width;
                if($colCount >= $cols){
                    if($sum!=12){
                    echo '<span class="sum red">'.$sum.'</span>';
                    }else{
                    echo '<span class="sum green">'.$sum.'</span>';
                    }
                    echo '</div>';
                    $colCount = 0;
                    $sum = 0;
                }
            }
            echo '</p>';
            $this->display_editors($items,$post_id);
        }
    }
    function save($post_id){
        $is_autosave = wp_is_post_autosave($post_id);
        $is_revision = wp_is_post_revision($post_id);
        $is_valid_nonce = isset($_POST[self::nonce_name]) && wp_verify_nonce($_POST[self::nonce_name],self::nonce_action) ? 'true' : 'false';
        $full_edit_mode = isset($_POST['full_edit_mode']);

        if ( $is_autosave || $is_revision || !$is_valid_nonce || !$full_edit_mode ) {
            return;
        }

        if(isset($_POST[self::prefix.'id']) && !empty($_POST[self::prefix.'id'])){
            update_post_meta($post_id,self::prefix.'id',sanitize_text_field($_POST[self::prefix.'id']));
        } else{
            $id = strtolower(get_the_title($post_id)) . '-' . $post_id;
            update_post_meta($post_id,self::prefix.'id',$id);
        }

        $cols = $_POST[self::prefix.'cols'];
        $rows = $_POST[self::prefix.'rows'];
        if(isset($cols)){
            update_post_meta($post_id,self::prefix.'cols',(int)sanitize_text_field($cols));
        }
        if(isset($rows)){
            update_post_meta($post_id,self::prefix.'rows',(int)sanitize_text_field($rows));
        }
        update_post_meta($post_id,self::prefix.'items',sanitize_text_field($cols*$rows));

        if(isset($_POST[self::prefix.'editor'])){
            foreach($_POST[self::prefix.'editor'] as $key => $value){
                update_post_meta($post_id,self::prefix.'editor_'.$key,apply_filters('the_content',$value));
            }
        }
        if(isset($_POST[self::prefix.'editor_width'])){
            foreach($_POST[self::prefix.'editor_width'] as $key => $value){
                update_post_meta($post_id,self::prefix.'editor_width_'.$key,sanitize_text_field($value));
            }
        }
        if(isset($_POST[self::prefix.'classes'])){
            update_post_meta($post_id,self::prefix.'classes',$_POST[self::prefix.'classes']);
        }
    }

	function save_used_in($post_id){
		$is_autosave = wp_is_post_autosave($post_id);
		$is_revision = wp_is_post_revision($post_id);

		if ( $is_autosave || $is_revision  ) {
			return;
		}
		if($this->get_current_post_type() != self::post_type){
			$p = get_post($post_id);

			$grids = get_posts(array('post_type' => self::post_type, 'posts_per_page' => -1));
			foreach($grids as $grid){
				delete_post_meta($grid->ID,'_used_in',$post_id);
			}
			$pattern = "/" . preg_quote('[grid name=', "/") . "([^\\]]*)"."/";
			if(preg_match_all( $pattern, $p->post_content, $matches )){
				foreach($matches[1] as $match){
					$m = str_replace('"','',$match);
					$grid_wp_id = $this->get_grid_wp_id($m);
					$used_in = get_post_meta($grid_wp_id,'_used_in');
					$update = true;
					if($used_in){
						foreach($used_in as $val){
							if($val == $post_id) $update = false;
						}
					}
					if($update)
					add_post_meta($grid_wp_id,'_used_in',$post_id);
				}
			}
		}
	}
    function get_set_id($post_id){
        $id = get_post_meta($post_id,self::prefix.'id',true);
        $html = '';
        $html .= '<label>' . __('Grid ID: ',self::text_domain) . '</label>';
        $html .= '<input type="text" name="'.self::prefix.'id'.'" value="'.$id.'" />';

        return $html;
    }
    function display_editors($items,$post_id){
        for($i = 0; $i < $items;$i++){
            echo '<p>';
            $name = self::prefix.'editor['.$i.']';
            $content = get_post_meta($post_id,self::prefix.'editor_'.$i,true);

	        $setup = array(
		        'setup' => 'function(ed) {
          ed.onChange.add(function(ed, l) {
                  updatePreview();
          }); }'
	        );

            wp_editor($content,'editor-'.$i,array(
                'textarea_name' => $name,
	            'tinymce' => $setup
            ));
            echo '</p>';
        }
    }
    function shortcode($atts){
        extract(shortcode_atts(array(
            'name' => '',
        ),$atts) );

        $post_id = self::get_grid_wp_id($name);

        if(!isset($post_id)) return;

        $cols = get_post_meta($post_id,self::prefix.'cols',true);
        $items = get_post_meta($post_id,self::prefix.'items',true);

        return $this->draw_grid($cols,$items,$post_id);
    }

    function draw_grid($cols, $items, $post_id){
        $html = '';
        $default_class = get_post_meta($post_id,self::prefix.'id',true);
        $optional_classes = get_post_meta($post_id,self::prefix.'classes',true);

        if(!empty($default_class)){
            $html .= '<div class="' . $default_class;
        }
        if(!empty($default_class) || !empty($optional_classes)) $html .= '">';
        for($i =  0,$colCount = 0; $i < (int)$items;$i++){
            if($colCount == 0) $html .= '<div class="row">';
			$col_width = get_post_meta($post_id,self::prefix.'editor_width_'.$i,true);
	        $col_width = (empty($col_width)) ? (int)(12/$cols) : $col_width;

            $html .= '<div class="large-'.$col_width.' columns ' . $optional_classes . '">';
            $html .= do_shortcode(get_post_meta($post_id,self::prefix.'editor_'.$i,true));
            $html .= '</div>';
            $colCount++;
            if($colCount >= (int) $cols){
                $html .= '</div>';
                $colCount = 0;
                continue;
            }
        }
        if(!empty($default_class)) $html .= '</div>';
        return $html;
    }

    function style(){
        ?>
        <style>
            label{
                display: inline-block;
                width: 200px;
            }
        </style>
    <?php
    }
    static function get_grid_wp_id($grid_id){
        $p = get_posts(array(
            'post_type' => self::post_type,
            'posts_per_page' => -1,
            'meta_key' => self::prefix.'id',
            'meta_value' => $grid_id,
        ));
        return $p[0]->ID;
    }
    function get_current_post_type() {
        global $post, $typenow, $current_screen;

        //we have a post so we can just get the post type from that
        if ( $post && $post->post_type )
            return $post->post_type;

        //check the global $typenow - set in admin.php
        elseif( $typenow )
            return $typenow;

        //check the global $current_screen object - set in sceen.php
        elseif( $current_screen && $current_screen->post_type )
            return $current_screen->post_type;

        //lastly check the post_type querystring
        elseif( isset( $_REQUEST['post_type'] ) )
            return sanitize_key( $_REQUEST['post_type'] );

        //we do not know the post type!
        return null;
    }
}