jQuery(document).ready(function($){

    $('.slider').each(function(){
        var value = parseInt( $( this ).find('input').val(), 12 );
        $(this).slider({
            value:value,
            min:1,
            max:12,
            step:1,
            create: function(){
                $(this).find('.width').appendTo($(this).find('.ui-slider-handle'));
            },
            slide:function(event, ui){
                $(this).find('input').val(ui.value);
                $(this).find('span').html(ui.value);

                var row = $(this).parent('.sliderRow');
                var sum = 0;
                row.find('input').each(function(){
                    sum += parseInt($(this).val());
                });
                row.find('.sum').html(sum);

                if(sum !=12){
                    row.find('.sum').removeClass('green');
                    row.find('.sum').addClass('red');
                }else{
                    row.find('.sum').addClass('green');
                    row.find('.sum').removeClass('red');
                }
            }

        });

    });
});
function updatePreview(){
    var editors = tinymce.editors;
    jQuery('#modal').find('.columns').each(function(i){
        jQuery(this).html(editors[i].getContent());
    });
}