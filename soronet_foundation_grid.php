<?php
/*
* Plugin Name: Soronet Foundation Grid
* Author: Tanel Kollamaa
* Description: Create Foundation framework grids in WP
* Version: 0.0
*/
define('SOROGRID_DIR',plugin_dir_url(__FILE__));
include('inc/soronet_foundation_grid.class.php');

function init_soronet_foundation_grid(){
	new soronet_foundation_grid();
}
add_action('init','init_soronet_foundation_grid');

function soronet_foundation_grid_asset_setup(){
	wp_enqueue_script('soronet-foundation-grid',SOROGRID_DIR .'inc/soronet_foundation_grid.js',array('jquery'));
    wp_enqueue_script('jquery-ui',SOROGRID_DIR.'inc/jquery-ui.js',array('jquery'));
	wp_enqueue_script('foundation-js',SOROGRID_DIR .'inc/foundation.min.js',array('jquery'));

    wp_enqueue_style('jquery-css',SOROGRID_DIR.'css/jquery-ui.css');
    wp_enqueue_style('plugin-css',SOROGRID_DIR.'css/plugin-style.css');
    wp_enqueue_style('admin-foundation',SOROGRID_DIR .'css/foundation.admin.min.css');
}
add_action('admin_enqueue_scripts','soronet_foundation_grid_asset_setup');

function sorogrid_foundation_css_setup(){
	if(get_option('load_foundation')){
		wp_enqueue_style('foundation',SOROGRID_DIR . '/css/foundation.min.css');
	}
}
add_action('wp_enqueue_scripts','sorogrid_foundation_css_setup');

function sorogrid_init_reveal(){
	?>
		<script>
			jQuery(document).foundation();
		</script>
	<?php
}
add_action('admin_footer','sorogrid_init_reveal');
